from flask import Flask

app = Flask(__name__)

@app.route('/calcular-media/<float:nota1>/<float:nota2>')
def calcular_media(nota1,nota2):
    media = (nota1 + nota2) / 2
    return f"A media do aluno é {media:.2f}."
    
if __name__ == '__main__':
    app.run(debug=True)
    
